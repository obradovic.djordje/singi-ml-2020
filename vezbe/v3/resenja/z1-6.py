import cv2
import numpy as np

# Ucitavanje i prikaz slike
slika = cv2.imread("slika1.png")
cv2.imshow("Pocetna slika", slika)

# Konverzija slike u binarnu sliku.
slika = cv2.cvtColor(slika, cv2.COLOR_BGR2GRAY)

# Pronalazenje povezanih komponenti.
broj_komponenti, komponente = cv2.connectedComponents(slika)
komponente = np.array(komponente, np.uint8)

# Bojenje komponenti razlicitim bojama.
obojene_komponente = np.zeros((komponente.shape[0], komponente.shape[1], 3), dtype=np.uint8)
for i in range(broj_komponenti-1):
    obojene_komponente[komponente == i+1, 0] = np.random.randint(50, 200)
    obojene_komponente[komponente == i+1, 1] = np.random.randint(50, 200)
    obojene_komponente[komponente == i+1, 2] = np.random.randint(50, 200)

cv2.imshow("Komponente", obojene_komponente)

kernel = np.array([[0, 1, 0],
                   [1, 1, 1],
                   [0, 1, 0]], np.uint8)

# Isprobati i druge kernele.
# kernel = np.ones((9, 9))
# kernel = np.ones((15, 15))

# Dilatacija i erozija obojene slike.
dilatacija_komponente = cv2.dilate(obojene_komponente, kernel)
erozija_komponente = cv2.erode(obojene_komponente, kernel)

cv2.imshow("Dilatacija komponente", dilatacija_komponente)
cv2.imshow("Erozija komponente", erozija_komponente)

# Pronalazenje ivica izdvojenih komponenti morfoloskim operacijama.
kernel = np.array([[0, 1, 0],
                   [1, 1, 1],
                   [0, 1, 0]], np.uint8)
# Isprobati i druge kernele.
erozija_komponente = cv2.erode(obojene_komponente, kernel)
ivice = obojene_komponente-erozija_komponente
cv2.imshow("Ivice", ivice)

# Racunanje nagiba i ekscentriciteta.
ekscentriciteti = []
for i in range(broj_komponenti-1):
    komponenta = np.array(komponente == i+1, dtype=np.uint8)
    momenti = cv2.moments(komponenta, binaryImage=True)
    ugao = np.rad2deg(0.5*np.arctan2(2*momenti["mu11"], (momenti["mu20"]-momenti["mu02"])))
    ekscentricitet = ((momenti["mu20"]-momenti["mu02"])**2 + 4*momenti["mu11"]**2)/(momenti["mu20"]+momenti["mu02"])**2
    ekscentriciteti.append(ekscentricitet)
    print("Kontura {}, nagib: {}, ekscentricitet {}.".format(i+1, ugao, ekscentricitet))

# Izdvajanje kruznih objekata.
krugovi = np.zeros((komponente.shape[0], komponente.shape[1], 3), dtype=np.uint8)
for i, e in enumerate(ekscentriciteti, 1):
    # Za kruzne objekte ekscentricitet tezi ka 0 dok za
    # elipsaste tezi 1.
    # Ako je ekscentricitet manji od 0.2 smatracemo da je objekat
    # dovoljno kruzan.
    if e < 0.1:
        krugovi[komponente == i] = [255, 0, 0]
cv2.imshow("Krugovi - ekscentricitet", krugovi)

# Pronalazenje krugova Hafovom transformacijom.
detektovani_krugovi = cv2.HoughCircles(slika, cv2.HOUGH_GRADIENT, 5, 20, maxRadius=60)
krugovi_haf = np.copy(obojene_komponente)# np.zeros(komponente.shape, dtype=np.uint8)
for krug in detektovani_krugovi[0]:
    krugovi_haf = cv2.circle(krugovi_haf, (krug[0], krug[1]), krug[2], (255, 0, 0), 3)

cv2.imshow("Krugovi - haf", krugovi_haf)
cv2.waitKey()
