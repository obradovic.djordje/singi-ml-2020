import numpy as np
from neuron import Neuron
from layer import Layer
from network import Network
from computation_graph import MSE, Sigmoid

print("Primer jedan neuron: ")
x = np.array([
    [5, 1], [6, 1], [7, 1], [8, 1]
])

y = [0, 0, 1, 1]

neuron = Neuron(2, Sigmoid())
err = MSE()

for j in range(5000):
    for i in range(len(x)):
        neuron.train(x[i], y[i], err)

for i in range(len(x)):
    print(neuron.forward(x[i]))

print("Primer jedan sloj: ")
x = np.array([
    [5, 1], [6, 1], [7, 1], [8, 1]
])

y = np.array([[0, 1], [0, 1], [1, 0], [1, 0]])

sloj = Layer(2, 2)
err = MSE()

for j in range(5000):
    for i in range(len(x)):
        sloj.train(x[i], y[i], err)

for i in range(len(x)):
    print(sloj.forward(x[i]))


print("Primer mreza: ")
x = np.array([
    [5, 1], [6, 1], [7, 1], [8, 1]
])

y = [0, 0, 1, 1]

network = Network()
network.add_layer(Layer(2, 2))
network.add_layer(Layer(2, 2))
network.add_layer(Layer(2, 1))
err = MSE()

for j in range(5000):
    for i in range(len(x)):
        network.train(x[i], y[i], err)

for i in range(len(x)):
    print(network.forward(x[i]))