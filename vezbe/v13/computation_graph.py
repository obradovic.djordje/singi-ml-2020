import numpy as np

class Sum:
    def __init__(self):
        pass

    def forward(self, x):
        self.x = x
        return np.sum(x)

    def backward(self, dz):
        return np.ones(len(self.x)) * dz

class Mul:
    def __init__(self):
        pass

    def forward(self, x):
        self.x = x
        return x[0]*x[1]

    def backward(self, dz):
        return np.array([self.x[1]*dz, self.x[0]*dz])

class Sigmoid:
    def __init__(self):
        pass

    def _sigmoid(self):
        return 1/(1+np.exp(-self.x))

    def forward(self, x):
        self.x = x
        return self._sigmoid()

    def backward(self, dz):
        return self._sigmoid()*(1-self._sigmoid())*dz

class MSE:
    def __init__(self):
        pass

    def forward(self, y, y_expected):
        self.y = y
        self.y_expected = y_expected
        return (y - y_expected)**2

    def backward(self, dz):
        return 2*(self.y-self.y_expected)*dz