import numpy as np
from layer import Layer

class Network:
    def __init__(self):
        self.slojevi = []

    def add_layer(self, layer):
        self.slojevi.append(layer)

    def forward(self, x):
        ulaz_u_sledeci = x
        for s in self.slojevi:
            ulaz_u_sledeci = s.forward(ulaz_u_sledeci)
        return ulaz_u_sledeci

    def backward(self, dz):
        ulaz_u_sledeci = dz
        for s in reversed(self.slojevi):
            ulaz_u_sledeci = s.backward(np.average(ulaz_u_sledeci, axis=0))
        return ulaz_u_sledeci

    def train(self, x, y, err):
        izlaz = self.forward(x)
        greska = err.forward(izlaz[0], y)
        izvod_greske = err.backward(1)
        ulaz_u_sledeci = [[izvod_greske]]
        for s in reversed(self.slojevi):
            ulaz_u_sledeci = s.backward(np.average(ulaz_u_sledeci, axis=0))
            for i, n in enumerate(s.neuroni):
                n.tezine -= ulaz_u_sledeci[i]*0.1
        return ulaz_u_sledeci