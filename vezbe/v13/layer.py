from computation_graph import Sigmoid
from neuron import Neuron

class Layer:
    def __init__(self, broj_ulaza, broj_neurona):
        self.neuroni = []
        for i in range(broj_neurona):
            self.neuroni.append(Neuron(broj_ulaza, Sigmoid()))

    def forward(self, x):
        izlaz = []
        for n in self.neuroni:
            izlaz.append(n.forward(x))
        return izlaz

    def backward(self, dz):
        izvodi = []
        for i, n in enumerate(self.neuroni):
            izvodi.append(n.backward(dz[i]))
        return izvodi

    def train(self, x, y, err):
        for i, n in enumerate(self.neuroni):
            n.train(x, y[i], err)