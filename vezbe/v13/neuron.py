import numpy as np
from computation_graph import Sum, Mul

class Neuron:
    def __init__(self, broj_ulaza, aktivacija):
        self.broj_ulaza = broj_ulaza
        self.tezine = np.random.rand(broj_ulaza)
        
        self.mnozenja = []
        
        for i in range(broj_ulaza):
            self.mnozenja.append(Mul())
        
        self.suma = Sum()
        self.aktivacija = aktivacija

    def forward(self, x):
        rezultati_mnozenja = []
        for i in range(len(x)):
            rezultati_mnozenja.append(self.mnozenja[i].forward([x[i], self.tezine[i]]))
        
        return self.aktivacija.forward(self.suma.forward(rezultati_mnozenja))

    def backward(self, dz):
        izvodi_mnozenja = []
        
        for m in self.mnozenja:
            izvodi_mnozenja.append(m.backward(dz)[1])

        return self.aktivacija.backward(self.suma.backward(izvodi_mnozenja))

    def train(self, x, y, err):
        izlaz = self.forward(x)
        greska = err.forward(izlaz, y)
        izvod_greske = err.backward(1)
        izvodi = self.backward(izvod_greske)
        self.tezine -= izvodi*0.1