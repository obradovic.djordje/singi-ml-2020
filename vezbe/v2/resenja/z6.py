import numpy as np
from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import train_test_split

# Iris (petunije) dataset sadrzi podatke o dimenzijama
# latica cveta i labelu kojoj vrsti cvet pripada.
# Ovi podaci su smesteni u datoteku iris.data u CSV formatu.
# Poslednja kolona u datoteci predstavlja labelu.
with open("iris.data") as fp:
    podaci_x = []
    podaci_y = []
    # Datoteka se ucitava red po red.
    for l in fp:
        # Na osnovu svakog reda se prave liste koje
        # sadrze brojeve ucitane iz reda.
        l = list(map(lambda v: float(v), l.split(",")))
        # Prva cetiri broja predstavljaju nezavisne promenljive.
        podaci_x.append(l[:-1])
        # Poslednji broj je zavisna promenljiva.
        podaci_y.append(l[-1])

    # Pretvaranje podataka u numpy nizove.
    podaci_x = np.array(podaci_x)
    podaci_y = np.array(podaci_y, dtype=np.int)

    # Podela podataka na obucavajuce i test skupove podataka.
    podaci_x_obuka, podaci_x_test, podaci_y_obuka, podaci_y_test = train_test_split(podaci_x, podaci_y, test_size=0.25)

    # Instanciranje klasifikatora.
    gnb = GaussianNB()
    # Obucavanje klasifikatora.
    gnb.fit(podaci_x_obuka, podaci_y_obuka)
    # Testiranje klasifikatora.
    print(gnb.score(podaci_x_test, podaci_y_test))
