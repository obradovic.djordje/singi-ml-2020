import numpy as np
from matplotlib import pyplot as plt

# Zadatak 1
def histogram(slika):
    slika = slika.reshape((slika.shape[0]*slika.shape[1]))
    # Histogram ce imati 256 polja, svako za po jednu nijansu
    # sive boje.
    hist = np.zeros(256)

    # Prebrojavanje pojava nijansi sive. Svaka pojava odredjene
    # nijanse povecava broj pojava u odgovarajucem polju.
    for p in slika:
        hist[int(p)] += 1

    return hist

# Zadatak 2
def srednja_vrednost(slika):
    return np.average(slika.reshape((slika.shape[0]*slika.shape[1])))

def varijansa(slika):
    srv = srednja_vrednost(slika)
    slika = slika.reshape((slika.shape[0]*slika.shape[1]))
    return np.sum((slika-srv)**2)/len(slika)

def standardna_devijacija(slika):
    return np.sqrt(varijansa(slika))

def normalna_raspodela(slika, norm=None):
    srv = srednja_vrednost(slika)
    std = standardna_devijacija(slika)
    if norm is None:
        norm = 1/std*np.sqrt(2*np.pi)
    def raspodela(x):
        return norm*np.exp(-0.5*((x-srv)/std)**2)
    return raspodela

# Zadatak 4
def verovatnoce(slika):
    hist = histogram(slika)
    return hist/np.sum(hist)

# Zadatak 5
def verovatnoca_vece_od(slika, x):
    slika = slika.reshape((slika.shape[0]*slika.shape[1]))
    return np.count_nonzero(slika > x)/len(slika)

with open("lenna.pgm") as im:
    # Prva dva reda predsavljaju format slike i komentar
    # ovi redovi se preskacu.
    im.readline()
    im.readline()
    # Treci red sadrzi dimenzije slike. Broj kolona i redova.
    dimenzije = tuple(map(lambda d: int(d), im.readline().split()))
    # Cetvrti red nosi podatka o maksimalnom intezitetu piksela na slici
    # i ovaj red ce biti preskocen.
    im.readline()
    # Nakon ovog reda dolaze pojedinacni pikseli slike koji ce biti
    # ucitani kao numpy niz.
    # Niz ce biti kreiran kao jednodimenzioni niz cija je duzina
    # jednaka sirina*visina slike. Ovaj niz ce inicijalno biti
    # popunjen nulama.
    slika = np.zeros(dimenzije[0]*dimenzije[1])
    # For petljom ce biti preuzet svaki piksel i
    # smesten u niz.
    for i, l in enumerate(im):
        slika[i] = int(l)
    # Dobijena slika je u memoriji smestena kao jednodimenzioni niz,
    # sliku je pre prikaza potrebno transformisati u matricu cije su
    # dimenzije jednake dimenzijama originalne slike.
    slika = slika.reshape(dimenzije)
    # Transformisanu sliku je sada moguce prikazati.
    plt.subplot(1, 3, 1)
    plt.imshow(slika)

    # Racunanje histograma.
    hist = histogram(slika)
    # Crtanje izracunatog histograma.
    plt.subplot(1, 3, 2)
    plt.hist(range(256), weights=hist, bins=range(256))

    # Racunanje raspodele, faktor za normalizaciju je jednak
    # maksimalnoj vrednosti u histogramu.
    raspodela = normalna_raspodela(slika, np.max(hist))
    x = np.linspace(-50, 300, 1000)
    plt.plot(x, raspodela(x))

    # Racunanje verovatnoca za sve intenzitete.
    print(verovatnoce(slika))

    # Racunanje verovatnoce da je piksel
    # intenziteta veceg od 127.
    print(verovatnoca_vece_od(slika, 127))

    plt.show()