import numpy as np
import struct

from matplotlib import pyplot as plt

from sklearn.decomposition import KernelPCA

def ucitaj_slike(putanja):
    with open(putanja, "rb") as fp:
        # Preskakanje magicnog broja.
        struct.unpack(">I", fp.read(4))
        # Ucitavanje broja slika.
        broj_slika = struct.unpack(">I", fp.read(4))[0]
        # Ucitavanje sirine slika.
        sirina = struct.unpack(">I", fp.read(4))[0]
        # Ucitavanje visine slika.
        visina = struct.unpack(">I", fp.read(4))[0]
        # Ucitavanje slika.
        slike = np.frombuffer(fp.read(broj_slika*sirina*visina), dtype=np.uint8)
        return slike.reshape((broj_slika, sirina, visina))

def ucitaj_labele(putanja):
    with open(putanja, "rb") as fp:
        # Preskakanje magicnog broja.
        struct.unpack(">I", fp.read(4))
        # Ucitavanje broja slika.
        broj_labela = struct.unpack(">I", fp.read(4))[0]
        # Ucitavanje labela.
        labele = np.frombuffer(fp.read(broj_labela), dtype=np.uint8)
        return labele

slike = ucitaj_slike("t10k-images.idx3-ubyte")
labele = ucitaj_labele("t10k-labels.idx1-ubyte")

# Redukcija na tri dimenzije.
pca = KernelPCA(n_components=3)
redukovano_train = pca.fit_transform(slike.reshape((len(slike), 28*28)))
fig = plt.figure()
axis = plt.axes(projection='3d')
axis.scatter3D(redukovano_train[:,0], redukovano_train[:,1], redukovano_train[:, 2], c=labele)
plt.show()

# Redukcija na dve dimenzije.
pca = KernelPCA(n_components=2)
redukovano_train = pca.fit_transform(slike.reshape((len(slike), 28*28)))
plt.scatter(redukovano_train[:, 0], redukovano_train[:, 1], c=labele)
plt.show()