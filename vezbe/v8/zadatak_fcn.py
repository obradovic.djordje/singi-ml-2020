import numpy as np
import struct

import torch
from torch import nn
import torch.nn.functional as F
from torch import optim
from torchvision import datasets
from torchvision import transforms
from torchvision import datasets
from torch.utils.data import Dataset
from torch.utils.data import DataLoader

from matplotlib import pyplot as plt

import random

# Definisanje klase za opis skupa podataka.
class MnistDataset(Dataset):
    def __init__(self, path=None):
        super().__init__()
        # Skladiste skupa podataka.
        self.data = None
        self._load(path)
        
    def ucitaj_slike(self, putanja):
        with open(putanja, "rb") as fp:
            # Preskakanje magicnog broja.
            struct.unpack(">I", fp.read(4))
            # Ucitavanje broja slika.
            broj_slika = struct.unpack(">I", fp.read(4))[0]
            # Ucitavanje sirine slika.
            sirina = struct.unpack(">I", fp.read(4))[0]
            # Ucitavanje visine slika.
            visina = struct.unpack(">I", fp.read(4))[0]
            # Ucitavanje slika.
            slike = np.frombuffer(fp.read(broj_slika*sirina*visina), dtype=np.uint8)
            return slike.reshape((broj_slika, sirina, visina)).astype(np.float32)/255

    def ucitaj_labele(self, putanja):
        with open(putanja, "rb") as fp:
            # Preskakanje magicnog broja.
            struct.unpack(">I", fp.read(4))
            # Ucitavanje broja slika.
            broj_labela = struct.unpack(">I", fp.read(4))[0]
            # Ucitavanje labela.
            labele = np.frombuffer(fp.read(broj_labela), dtype=np.uint8)
            return labele.astype(np.int64)

    def _load(self, path):
        images_path = path + "-images.idx3-ubyte"
        labels_path = path + "-labels.idx1-ubyte"
        images = self.ucitaj_slike(images_path)
        labels = self.ucitaj_labele(labels_path)
        self.data = list(zip(images.reshape((len(images), 28*28)), labels))
        
    def __getitem__(self, index):
        return self.data[index]
    
    def __len__(self):
        return len(self.data)

test_ds = MnistDataset("t10k")
train_ds = MnistDataset("train")

train_loader = DataLoader(train_ds, batch_size=32, shuffle=True)
test_loader = DataLoader(test_ds, batch_size=32, shuffle=False)

img = iter(train_loader)

plt.imshow(img.next()[0][0].numpy().transpose(1, 2, 0))
plt.show()

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(3, 5, 5, padding=2)
        self.conv2 = nn.Conv2d(5, 10, 3, padding=1)
        self.conv3 = nn.Conv2d(10, 20, 3, padding=1)
        self.conv4 = nn.Conv2d(20, 40, 3, padding=1)
        self.conv5 = nn.Conv2d(20, 10, 3, padding=1)

    def forward(self, x):
        x = self.conv1(x)
        x = self.conv2(x)
        x = self.conv3(x)
        x = self.conv4(x)
        x = self.conv5(x)
        return x

print(torch.cuda.is_available())
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
net = Net()
net.to(device)

criterion = nn.MSELoss()
optimizer = optim.RMSprop(net.parameters(), lr=0.001)
criterion.to(device)

for epoch in range(10):
    running_loss = 0.0
    for i, data in enumerate(train_loader):
        inputs, labels = data[0].to(device), data[1].to(device)
        optimizer.zero_grad()
        outputs = net(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()
        running_loss += loss.item()
    print("Loss: {:.3f}".format(running_loss))
    print("Gotova epoha {}".format(epoch+1))

print("Obucavanje zavrseno.")

total = 0
correct = 0

with torch.no_grad():
    for data in test_loader:
        features, labels = data[0].to(device), data[1].to(device)
        outputs = net(features)
        _, predicted = torch.max(outputs.data, 1)
        total += labels.size(0)
        correct += (predicted == labels).sum().item()

print("Tacnost: {:.3f}%".format(100 * correct / total))