import numpy as np

class Neuron:
    def __init__(self, broj_ulaza, aktivacion_funkcija):
        self.broj_ulaza = broj_ulaza
        self.tezine = np.random.rand(broj_ulaza)
        self.aktivacion_funkcija = aktivacion_funkcija

    def forward(self, x):
        return self.aktivacion_funkcija(np.sum(x*self.tezine))

    def backward(self, x):
        izvodi = []
        for i in range(len(x)):
            izvodi.append(x[i]*sigmoid_izvod(np.sum(x*self.tezine)))
        return np.array(izvodi)

def sigmoid(x):
    return 1/(1+np.exp(-x))

def sigmoid_izvod(x):
    return sigmoid(x)*(1-sigmoid(x))

def err(neuron_y, y):
    return (neuron_y - y)**2

def err_izvod(neuron_y, y):
    return 2*(neuron_y - y)

n = Neuron(1, sigmoid)
print(n.tezine)

x = np.array([
    [5, 1], [6, 1], [7, 1], [8, 1]
])

y = [0, 0, 1, 1]

for j in range(5000):
    for i in range(len(x)):
        izlaz = n.forward(x[i])
        greska = err(izlaz, y[i])
        izvod_greske = err_izvod(izlaz, y[i])
        izvod_mreze = n.backward(x[i])
        izvod = izvod_mreze*izvod_greske
        n.tezine = n.tezine - izvod*0.1

for i in range(len(x)):
    print(n.forward(x[i]))