import numpy as np
import struct

import cv2

from sklearn.cluster import KMeans
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import LinearSVC

from matplotlib import pyplot as plt

def ucitaj_slike(putanja):
    with open(putanja, "rb") as fp:
        # Preskakanje magicnog broja.
        struct.unpack(">I", fp.read(4))
        # Ucitavanje broja slika.
        broj_slika = struct.unpack(">I", fp.read(4))[0]
        # Ucitavanje sirine slika.
        sirina = struct.unpack(">I", fp.read(4))[0]
        # Ucitavanje visine slika.
        visina = struct.unpack(">I", fp.read(4))[0]
        # Ucitavanje slika.
        slike = np.frombuffer(fp.read(broj_slika*sirina*visina), dtype=np.uint8)
        return slike.reshape((broj_slika, sirina, visina))

def ucitaj_labele(putanja):
    with open(putanja, "rb") as fp:
        # Preskakanje magicnog broja.
        struct.unpack(">I", fp.read(4))
        # Ucitavanje broja slika.
        broj_labela = struct.unpack(">I", fp.read(4))[0]
        # Ucitavanje labela.
        labele = np.frombuffer(fp.read(broj_labela), dtype=np.uint8)
        return labele

slike = ucitaj_slike("t10k-images.idx3-ubyte")
labele = ucitaj_labele("t10k-labels.idx1-ubyte")


# Pretvaranje slika u jednodimenzione vektore.
vektori = slike.reshape(slike.shape[0], slike.shape[1]*slike.shape[2])

# 1. zadatak
# K Means za pronalazenje 10 klastera.
# Ocekivano je da ce u svakom mlasteru biti slike koje predstavljaju jednu cifru.
km = KMeans(10)

# Pronalazenje centara klastra.
labele_km = km.fit_predict(vektori)

# Iscrtavanje centara klastera.
for i, c in enumerate(km.cluster_centers_):
    plt.subplot(3, 10, i+1)
    plt.imshow(c.reshape((28, 28)))

# Iscrtavanje uprosecenih slika iz svakog klastera.
for i in range(10):
    prosek = (np.sum(vektori[labele_km==i], axis=0)/len(vektori)).reshape((28, 28))
    plt.subplot(3, 10, i+11)
    plt.imshow(prosek)

# Iscrtavanje nasumice odabranih uzoraka iz svakog klastera:
for i in range(10):
    slika = ((vektori[labele_km==i])[np.random.randint(0, np.count_nonzero(labele_km==i))]).reshape((28, 28))
    plt.subplot(3, 10, i+21)
    plt.imshow(slika)

plt.show()

# Ucitavanje podataka za obucavanje.
slike_train = ucitaj_slike("train-images.idx3-ubyte")
labele_train = ucitaj_labele("train-labels.idx1-ubyte")

vektori_train = slike_train.reshape(slike_train.shape[0], slike_train.shape[1]*slike_train.shape[2])

# 2. zadatak
# Klasifikator ce biti sacinjen od 10 linearnih kombinacija.
# Linearne kombinacije ce biti predstavljene matricom tezina,
# svaki red predstavlja po jedan vektor tezina.
# Inicijalno tezine ce imati vrednost 0.
class Klasifikator:
    def __init__(self, alpha=0.0001):
        self.tezine = None
        self.alpha = alpha

    def fit(self, x, y, max_iter=1000):
        # Pocetna iteracija.
        iter = 0
        # Postavljanje tezina na vrednost 0.
        self.tezine = np.zeros((len(np.unique(y)), len(x[0])))
        
        # Pravljenje matrice ciljnih vrednosti.
        ciljne_vrednosti = np.zeros((len(np.unique(y)), len(y)))
        # Popunjavanje matrice ciljnih vrednosti.
        for i, v in enumerate(y):
            ciljne_vrednosti[v,i] = 1
        
        # Obucavanje.
        while(iter < max_iter):
            # Racunanje klasa na osnovu zadatog ulaza.
            predikcije = self._predict(x)
            # Racunanje razlike ciljnih vrednosti i predvidjenih vrednosti.
            delta = ciljne_vrednosti - predikcije
            
            # Azuriranje tezina.
            self.tezine += self.alpha * (delta @ x)
            
            iter += 1

    def _predict(self, x):
        return (self.tezine @ x.T)>0

    def predict(self, x):
        return self._predict(x).argmax(axis=0)

    def score(self, x, y):
        predikcije = self.predict(x)
        return np.count_nonzero(predikcije == y)/len(x)

klasifikator = Klasifikator()
klasifikator.fit(vektori_train, labele_train)

# Provera nad test podacima.
print("Tacnost klasifikatora:", klasifikator.score(vektori, labele))

# 3. zadatak
knn = KNeighborsClassifier()
knn.fit(vektori_train, labele_train)

# Provera nad test podacima.
print("Tacnost KNN klasifikatora:", knn.score(vektori, labele))

# 4. zadatak
svm = LinearSVC()
svm.fit(vektori_train, labele_train)
svm_score = svm.score(vektori, labele)

# Provera nad test podacima.
print("Tacnost SVM klasifikatora:", svm_score)