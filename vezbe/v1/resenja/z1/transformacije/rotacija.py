import numpy as np

def rotiraj_naivno(slika, ugao):
    # Pretvaranje ugla u radijane.
    ugao = np.deg2rad(ugao)

    # Pravljenje matrice rotacije.
    matrica_rotacije = np.array([[np.cos(ugao), -np.sin(ugao)],
                                 [np.sin(ugao),  np.cos(ugao)]])
    
    # Racunanje dimenzija rotirane slike.
    # Prvo se racunaju temena slike zarotirana oko centra slike.
    centar = (slika.shape[1]/2-0.5, slika.shape[0]/2-0.5) # Centar slike.
    #                       x           y
    temena = np.array([[-centar[0], -centar[1]],
                       [ centar[0], -centar[1]],
                       [ centar[0],  centar[1]],
                       [-centar[0],  centar[1]]])
    # Rotiranje temena mnozenjem matrice temena matricom rotacije.
    rotirana_temena = temena @ matrica_rotacije

    # Racunanje novih dimenzija slike na osnovu rotiranih temena.
    nova_sirina = int(np.ceil(np.max(rotirana_temena[:,0]) - np.min(rotirana_temena[:,0])))
    nova_visina = int(np.ceil(np.max(rotirana_temena[:,1]) - np.min(rotirana_temena[:,1])))
    rotirana_slika = np.zeros((nova_sirina, nova_visina))

    # Racunanje centra rotirane slike.
    centar_rotirane_slike = (nova_visina/2-0.5, nova_sirina/2-0.5)

    for i in range(slika.shape[0]):
        for j in range(slika.shape[1]):
            zarotiran_piksel = np.array([j-centar[0], i-centar[1]]) @ matrica_rotacije
            rotirana_slika[int(np.round(zarotiran_piksel[1]+centar_rotirane_slike[1]))][int(np.round(zarotiran_piksel[0]+centar_rotirane_slike[0]))] = slika[i][j]
    return rotirana_slika

def rotiraj(slika, ugao):
    # Pretvaranje ugla u radijane.
    ugao = np.deg2rad(ugao)

    # Pravljenje matrice rotacije.
    matrica_rotacije = np.array([[np.cos(ugao), -np.sin(ugao)],
                                 [np.sin(ugao),  np.cos(ugao)]])
    
    # Racunanje dimenzija rotirane slike.
    # Prvo se racunaju temena slike zarotirana oko centra slike.
    centar = (slika.shape[1]/2-0.5, slika.shape[0]/2-0.5) # Centar slike.
    #                       x           y
    temena = np.array([[-centar[0], -centar[1]],
                       [ centar[0], -centar[1]],
                       [ centar[0],  centar[1]],
                       [-centar[0],  centar[1]]])
    # Rotiranje temena mnozenjem matrice temena matricom rotacije.
    rotirana_temena = temena @ matrica_rotacije

    # Racunanje novih dimenzija slike na osnovu rotiranih temena.
    nova_sirina = int(np.ceil(np.max(rotirana_temena[:,0]) - np.min(rotirana_temena[:,0])))
    nova_visina = int(np.ceil(np.max(rotirana_temena[:,1]) - np.min(rotirana_temena[:,1])))
    rotirana_slika = np.zeros((nova_sirina, nova_visina))

    # Racunanje centra rotirane slike.
    centar_rotirane_slike = (nova_visina/2-0.5, nova_sirina/2-0.5)

    for i in range(rotirana_slika.shape[0]):
        for j in range(rotirana_slika.shape[1]):
            zarotiran_piksel = np.array([j-centar_rotirane_slike[0], i-centar_rotirane_slike[1]]) @ matrica_rotacije.T
            ii = int(np.round(zarotiran_piksel[1]+centar[1]))
            jj = int(np.round(zarotiran_piksel[0]+centar[0]))
            if ii >= 0 and ii < slika.shape[0] and jj >= 0 and jj < slika.shape[0]:
                rotirana_slika[i][j] = slika[ii][jj]
    return rotirana_slika