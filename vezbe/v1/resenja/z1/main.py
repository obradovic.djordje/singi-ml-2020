import numpy as np
from matplotlib import pyplot as plt

from transformacije.rotacija import rotiraj_naivno, rotiraj

with open("lenna.pgm") as im:
    # Prva dva reda predsavljaju format slike i komentar
    # ovi redovi se preskacu.
    im.readline()
    im.readline()
    # Treci red sadrzi dimenzije slike. Broj kolona i redova.
    dimenzije = tuple(map(lambda d: int(d), im.readline().split()))
    # Cetvrti red nosi podatka o maksimalnom intezitetu piksela na slici
    # i ovaj red ce biti preskocen.
    im.readline()
    # Nakon ovog reda dolaze pojedinacni pikseli slike koji ce biti
    # ucitani kao numpy niz.
    # Niz ce biti kreiran kao jednodimenzioni niz cija je duzina
    # jednaka sirina*visina slike. Ovaj niz ce inicijalno biti
    # popunjen nulama.
    slika = np.zeros(dimenzije[0]*dimenzije[1])
    # For petljom ce biti preuzet svaki piksel i
    # smesten u niz.
    for i, l in enumerate(im):
        slika[i] = int(l)
    # Dobijena slika je u memoriji smestena kao jednodimenzioni niz,
    # sliku je pre prikaza potrebno transformisati u matricu cije su
    # dimenzije jednake dimenzijama originalne slike.
    slika = slika.reshape(dimenzije)
    # Transformisanu sliku je sada moguce prikazati.
    plt.subplot(1, 3, 1)
    plt.imshow(slika)
    # Prilikom rotiranja slike naivnom implementacijom dobice se crni
    # pikseli na koje nije namapiran ni jedan piksel originalne slike.
    plt.subplot(1, 3, 2)
    plt.imshow(rotiraj_naivno(slika, 32))
    # Slozenija implementacija nema prethodno navedeni problem.
    plt.subplot(1, 3, 3)
    plt.imshow(rotiraj(slika, 32))
    plt.show()