import numpy as np
from matplotlib import pyplot as plt

from regresija.regresija import linearna_regresija

# Generisanje inicijalnog skupa podataka.
uzorci_x = np.linspace(0, 10, 1000).reshape(1000,1)
uzorci_y = uzorci_x + (uzorci_x**2)+np.random.random((1000, 1))*25 + ((uzorci_x-5)**3)+np.random.random((1000, 1))*25

# Generisanje vektora nezavisnih promenljivih dopunjenih sa x^2 i x^3.
uzorci_x2 = np.array([np.ones(1000), uzorci_x[:,0], uzorci_x[:,0]**2])
uzorci_x3 = np.array([np.ones(1000), uzorci_x[:,0], uzorci_x[:,0]**2, uzorci_x[:,0]**3])

# Racunanje modela
lr1 = linearna_regresija(uzorci_x, uzorci_y)
lr2 = linearna_regresija(uzorci_x2.T, uzorci_y)
lr3 = linearna_regresija(uzorci_x3.T, uzorci_y)

# Prikaz svih tacaka.
plt.scatter(uzorci_x, uzorci_y)

# Generisanje ulaznih vektora x.
x = np.linspace(0, 10, 1000)
x2 = np.array([np.ones(1000), x, x**2])
x3 = np.array([np.ones(1000), x, x**2, x**3])

# Crtanje funkcije dobijene linearnom regresijom.
plt.plot(x, lr1(x))
plt.plot(x, lr2(x2))
plt.plot(x, lr3(x3))
plt.show()