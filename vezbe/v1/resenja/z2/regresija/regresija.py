import numpy as np

def linearna_regresija(x, y):
    # Racunanje tezina na osnovu zadatog x i y.
    tezine = (np.linalg.inv((x.T @ x)) @ x.T) @ y
    # Funkcija koja se vraca kao rezultat racunanja
    # linearne regresije, odnosno koja se formira
    # no osnovu izracunatih koeficijenata.
    def f(x):
        return np.sum(tezine*x, axis=0)
    return f