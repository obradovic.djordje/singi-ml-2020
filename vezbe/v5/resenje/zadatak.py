import numpy as np
import struct

import torch
from torch import nn
import torch.nn.functional as F
from torch import optim
from torchvision import transforms
from torchvision import datasets
from torch.utils.data import Dataset
from torch.utils.data import DataLoader

from matplotlib import pyplot as plt

import random

# Definisanje klase za opis skupa podataka.
class MnistDataset(Dataset):
    def __init__(self, path=None):
        super().__init__()
        # Skladiste skupa podataka.
        self.data = None
        self._load(path)
        
    def ucitaj_slike(self, putanja):
        with open(putanja, "rb") as fp:
            # Preskakanje magicnog broja.
            struct.unpack(">I", fp.read(4))
            # Ucitavanje broja slika.
            broj_slika = struct.unpack(">I", fp.read(4))[0]
            # Ucitavanje sirine slika.
            sirina = struct.unpack(">I", fp.read(4))[0]
            # Ucitavanje visine slika.
            visina = struct.unpack(">I", fp.read(4))[0]
            # Ucitavanje slika.
            slike = np.frombuffer(fp.read(broj_slika*sirina*visina), dtype=np.uint8)
            return slike.reshape((broj_slika, sirina, visina)).astype(np.float32)/255

    def ucitaj_labele(self, putanja):
        with open(putanja, "rb") as fp:
            # Preskakanje magicnog broja.
            struct.unpack(">I", fp.read(4))
            # Ucitavanje broja slika.
            broj_labela = struct.unpack(">I", fp.read(4))[0]
            # Ucitavanje labela.
            labele = np.frombuffer(fp.read(broj_labela), dtype=np.uint8)
            return labele.astype(np.int64)

    def _load(self, path):
        images_path = path + "-images.idx3-ubyte"
        labels_path = path + "-labels.idx1-ubyte"
        images = self.ucitaj_slike(images_path)
        labels = self.ucitaj_labele(labels_path)
        self.data = list(zip(images.reshape((len(images), 28*28)), labels))
        
    def __getitem__(self, index):
        return self.data[index]
    
    def __len__(self):
        return len(self.data)

test_ds = MnistDataset("t10k")
train_ds = MnistDataset("train")

test_loader = DataLoader(test_ds, shuffle=True, batch_size=32)
train_loader = DataLoader(train_ds, shuffle=True, batch_size=32)

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(784, 500)
        self.fc2 = nn.Linear(500, 250)
        self.fc3 = nn.Linear(250, 125)
        self.fc4 = nn.Linear(125, 10)

    def forward(self, x):
        x = F.tanh(self.fc1(x))
        x = F.tanh(self.fc2(x))
        x = F.tanh(self.fc3(x))
        x = F.tanh(self.fc4(x))
        return x
print(torch.cuda.is_available())

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
net = Net()
net.to(device)

criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)
criterion.to(device)

for epoch in range(25):
    running_loss = 0.0
    for i, data in enumerate(train_loader):
        inputs, labels = data[0].to(device), data[1].to(device)
        optimizer.zero_grad()
        outputs = net(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()
        running_loss += loss.item()
        if i % 25 == 24:
            print("Loss: {:.3f}".format(running_loss/25))
            running_loss = 0.0
    print("Gotova epoha {}".format(epoch+1))

print("Obucavanje zavrseno.")

total = 0
correct = 0

with torch.no_grad():
    for data in test_loader:
        features, labels = data[0].to(device), data[1].to(device)
        outputs = net(features)
        _, predicted = torch.max(outputs.data, 1)
        total += labels.size(0)
        correct += (predicted == labels).sum().item()

print("Tacnost: {:.3f}%".format(100 * correct / total))

# Zadatak 3.

# Pravljenje velike slike
dimenzije = (200, 300)
slika = np.zeros(dimenzije)
uzorci = random.choices(test_ds.data, k=5)
for u in uzorci:
    x = random.randint(0, dimenzije[0]-28)
    y = random.randint(0, dimenzije[1]-28)
    slika[x:x+28, y:y+28] = u[0].reshape((28,28))

# Na izlazu ce biti onoliko slika koliko ima klasa.
rezultati = np.zeros((dimenzije[0], dimenzije[1], 10))

# Inicijalna pozicija prozora
with torch.no_grad():
    prozori = []
    koordinate = []
    for i in range(slika.shape[0]-28):
        for j in range(slika.shape[1]-28):
            prozor = slika[i:i+28, j:j+28]
            koordinate.append((i, j))
            prozori.append(prozor.reshape((28*28,)))
    prozori = np.array(prozori)
    outputs = net(torch.from_numpy(prozori).float().to(device))
    # Smatramo da je predikcija tacna ako je sigurnost veca od 0.95.
    # Ovo bi bilo bolje da se radi u batchevima tako sto se prvo
    # pokupe prozori pa onda puste svi prozori na obradu. Ili se pokupi neki broj
    # prozora pa se prosledi na obradu.
    # Takodje bilo bi dobro uvesti dodatnu klasu koja predstavlja sve
    # sto nije broj.
    # print(outputs)
    for k, output in enumerate(outputs):
        if torch.max(output) > 0.95:
            rezultati[koordinate[k][0]:koordinate[k][0]+28, koordinate[k][1]:koordinate[k][1]+28, torch.argmax(output)] += torch.max(output).item()

plt.subplot(3, 5, 1)
plt.imshow(slika)
for i in range(10):
    plt.subplot(3, 5, i+6)
    plt.imshow(rezultati[:,:,i])

plt.show()