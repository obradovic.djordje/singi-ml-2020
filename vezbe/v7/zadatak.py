import numpy as np
import struct

import torch
from torch import nn
import torch.nn.functional as F
from torch import optim
from torchvision import datasets
from torchvision import transforms
from torchvision import datasets
from torch.utils.data import Dataset
from torch.utils.data import DataLoader

from matplotlib import pyplot as plt

import random

transformacije = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
])

train_ds = datasets.CIFAR10(root="ds", train=True, download=False, transform=transformacije)
test_ds = datasets.CIFAR10(root="ds", train=False, download=False, transform=transformacije)
train_loader = DataLoader(train_ds, batch_size=32, shuffle=True)
test_loader = DataLoader(test_ds, batch_size=32, shuffle=False)

img = iter(train_loader)

plt.imshow(img.next()[0][0].numpy().transpose(1, 2, 0))
plt.show()

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(3, 5, 3)
        self.conv2 = nn.Conv2d(5, 10, 3)
        self.mp = nn.MaxPool2d(2, 2)
        self.fc1 = nn.Linear(10*6*6, 200)
        self.fc2 = nn.Linear(200, 100)
        self.fc3 = nn.Linear(100, 10)

    def forward(self, x):
        x = self.mp(F.relu(self.conv1(x)))
        x = self.mp(F.relu(self.conv2(x)))
        x = x.view(-1, 10*6*6)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x
print(torch.cuda.is_available())
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
net = Net()
net.to(device)

criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)
criterion.to(device)

for epoch in range(10):
    running_loss = 0.0
    for i, data in enumerate(train_loader):
        inputs, labels = data[0].to(device), data[1].to(device)
        optimizer.zero_grad()
        outputs = net(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()
        running_loss += loss.item()
    print("Loss: {:.3f}".format(running_loss))
    print("Gotova epoha {}".format(epoch+1))

print("Obucavanje zavrseno.")

total = 0
correct = 0

with torch.no_grad():
    for data in test_loader:
        features, labels = data[0].to(device), data[1].to(device)
        outputs = net(features)
        _, predicted = torch.max(outputs.data, 1)
        total += labels.size(0)
        correct += (predicted == labels).sum().item()

print("Tacnost: {:.3f}%".format(100 * correct / total))