import cv2
import numpy as np
import random

from matplotlib import pyplot as plt

from loader import ucitaj_slike

slike = ucitaj_slike("t10k-images-idx3-ubyte")

broj_slika_za_generisanje = 10

for i in range(broj_slika_za_generisanje):
    slika_velika = np.zeros((600, 800))#np.random.randint(300, 900, (2,)))
    slika_labele = np.zeros((600, 800))
    for j in range(random.randint(3, 10)):
        slika_ds = random.choice(slike)
        x = random.randint(0, slika_velika.shape[0]-slika_ds.shape[0])
        y = random.randint(0, slika_velika.shape[1]-slika_ds.shape[1])
        slika_velika[x:x+slika_ds.shape[0], y:y+slika_ds.shape[1]] = slika_ds
        slika_labele[x:x+slika_ds.shape[0], y:y+slika_ds.shape[1]] = np.ones((28, 28))*255
    
    cv2.imwrite("slika_{}.png".format(i), slika_velika)
    cv2.imwrite("slika_labele_{}.png".format(i), slika_labele)
    