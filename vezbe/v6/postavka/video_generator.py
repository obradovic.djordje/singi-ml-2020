import cv2
import numpy as np
import random

from matplotlib import pyplot as plt

from loader import ucitaj_slike

slike = ucitaj_slike("t10k-images-idx3-ubyte")

broj_slika_za_generisanje = random.randint(3, 10)
rezolucija = (random.randint(300, 900), random.randint(300, 900))

code = cv2.VideoWriter_fourcc(*"MP42")
video_output = cv2.VideoWriter("./video.avi", code, 60.0, rezolucija)

class Slika:
    def __init__(self, slika_ds):
        self.slika_ds = slika_ds
        self._verovatnoca_promene = 0.01
        self.pozicija_delta = np.array([0, 0], dtype=np.int)
        self.pozicija = np.array([random.randint(0, rezolucija[0]), random.randint(0, rezolucija[1])], dtype=np.int)

    def promeni_pozicija_delta(self):
        if np.random.random() < self._verovatnoca_promene:
            self.pozicija_delta = np.random.randint(-3, 4, 2, dtype=np.int)
            self._verovatnoca_promene = 0.01
        else:
            self._verovatnoca_promene += 0.01

    def crtaj(self, slika):
        self.promeni_pozicija_delta()
        self.pozicija += self.pozicija_delta
        x1 = max(0, min(self.pozicija[0], slika.shape[0]-28))
        y1 = max(0, min(self.pozicija[1], slika.shape[1]-28))

        slika[x1:x1+self.slika_ds.shape[0], y1:y1+self.slika_ds.shape[1], 0] = self.slika_ds
        slika[x1:x1+self.slika_ds.shape[0], y1:y1+self.slika_ds.shape[1], 1] = self.slika_ds
        slika[x1:x1+self.slika_ds.shape[0], y1:y1+self.slika_ds.shape[1], 2] = self.slika_ds

slike_ds = []

for i in range(broj_slika_za_generisanje):
    slike_ds.append(Slika(random.choice(slike)))

for i in range(60*15):
    frame = np.random.randint(0, 256, (rezolucija[1], rezolucija[0], 3), dtype=np.uint8)
    for s in slike_ds:
        s.crtaj(frame)
    video_output.write(frame)

video_output.release()