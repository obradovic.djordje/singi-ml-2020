import numpy as np
import struct

def ucitaj_slike(putanja):
    with open(putanja, "rb") as fp:
        # Preskakanje magicnog broja.
        struct.unpack(">I", fp.read(4))
        # Ucitavanje broja slika.
        broj_slika = struct.unpack(">I", fp.read(4))[0]
        # Ucitavanje sirine slika.
        sirina = struct.unpack(">I", fp.read(4))[0]
        # Ucitavanje visine slika.
        visina = struct.unpack(">I", fp.read(4))[0]
        # Ucitavanje slika.
        slike = np.frombuffer(fp.read(broj_slika*sirina*visina), dtype=np.uint8)
        return slike.reshape((broj_slika, sirina, visina)).astype(np.float32)

def ucitaj_labele(putanja):
    with open(putanja, "rb") as fp:
        # Preskakanje magicnog broja.
        struct.unpack(">I", fp.read(4))
        # Ucitavanje broja slika.
        broj_labela = struct.unpack(">I", fp.read(4))[0]
        # Ucitavanje labela.
        labele = np.frombuffer(fp.read(broj_labela), dtype=np.uint8)
        return labele.astype(np.int64)