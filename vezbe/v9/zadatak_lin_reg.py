import numpy as np
from matplotlib import pyplot as plt

x = [
    [0, 0],
    [0, 1],
    [1, 0],
    [1, 1]
]

y = [
    1,
    0,
    0,
    1
]

# x=[
#     [1, 1],
#     [2, 1],
#     [3, 1],
# ]

# y = [
#     2,
#     3,
#     4
# ]

# t = np.linspace(-4, 4, 1000)

# x = np.array([
#     np.ones(1000),
#     t,
#     t**2,
#     t**3,
# ])

# x_prikaz = np.array([
#     np.ones(1000),
#     t,
#     t**2,
#     t**3,
# ])

# x_prikaz = x_prikaz.T
# x = x.T

# y = ((t + t**2 + t**3)+np.random.rand(1000)*25) + 10

# exit()
def lin_reg(x, y, delta=0.0001):
    tezine = np.random.rand(len(x[0]))
    print(tezine)
    def eval(x, tezine=tezine):
        dobijeni_izlaz = 0
        for j, komponenta in enumerate(x):
            dobijeni_izlaz += komponenta*tezine[j]
        return dobijeni_izlaz

    for e in range(10000):
        for ulaz, ocekivani_izlaz in zip(x, y):
            razlika = ocekivani_izlaz - eval(ulaz)

            for t in range(len(tezine)):
                kopija_tezina = np.copy(tezine)
                kopija_tezina[t] += delta
                nova_razlika = ocekivani_izlaz - eval(ulaz, kopija_tezina)
                if abs(nova_razlika) < abs(razlika):
                    tezine[t] += delta
                
                kopija_tezina = np.copy(tezine)
                kopija_tezina[t] -= delta
                nova_razlika = ocekivani_izlaz - eval(ulaz, kopija_tezina)

                if abs(nova_razlika) < abs(razlika):
                    tezine[t] -= delta
                
    print(tezine)
    return eval

def lin_reg_sgd(x, y, lr=0.00001):
    tezine = np.random.rand(len(x[0]))
    print(tezine)
    def eval(x, tezine=tezine):
        dobijeni_izlaz = 0
        for j, komponenta in enumerate(x):
            dobijeni_izlaz += komponenta*tezine[j]
        return dobijeni_izlaz

    for e in range(10000):
        for ulaz, ocekivani_izlaz in zip(x, y):
            razlika = eval(ulaz) - ocekivani_izlaz
            izvod_greske = 2*(razlika)
            for i in range(len(tezine)):
                tezine[i] = tezine[i] - lr*izvod_greske*ulaz[i]
    
    return eval

# print("model 1")
# model = lin_reg(x, y)
# print("model 2")
# model2 = lin_reg_sgd(x, y)

# for i, input_x in enumerate(x):
#     print(model2(input_x), "\t", y[i])

# plt.scatter(t, y)
# plt.plot(t, list(map(lambda x_in: model(x_in), x)), 'r')
# plt.show()

# plt.scatter(t, y)
# plt.plot(t, list(map(lambda x_in: model2(x_in), x_prikaz)), 'r')
# plt.show()
