import numpy as np
import struct

import torch
from torch import nn
import torch.nn.functional as F
from torch import optim
from torchvision import transforms
from torchvision import datasets
from torch.utils.data import Dataset
from torch.utils.data import DataLoader

from matplotlib import pyplot as plt

import random
from itertools import chain

# Definisanje klase za opis skupa podataka.
class MnistDataset(Dataset):
    def __init__(self, path=None):
        super().__init__()
        # Skladiste skupa podataka.
        self.data = None
        self._load(path)
        
    def ucitaj_slike(self, putanja):
        with open(putanja, "rb") as fp:
            # Preskakanje magicnog broja.
            struct.unpack(">I", fp.read(4))
            # Ucitavanje broja slika.
            broj_slika = struct.unpack(">I", fp.read(4))[0]
            # Ucitavanje sirine slika.
            sirina = struct.unpack(">I", fp.read(4))[0]
            # Ucitavanje visine slika.
            visina = struct.unpack(">I", fp.read(4))[0]
            # Ucitavanje slika.
            slike = np.frombuffer(fp.read(broj_slika*sirina*visina), dtype=np.uint8)
            return slike.reshape((broj_slika, sirina, visina)).astype(np.float32)/255

    def ucitaj_labele(self, putanja):
        with open(putanja, "rb") as fp:
            # Preskakanje magicnog broja.
            struct.unpack(">I", fp.read(4))
            # Ucitavanje broja slika.
            broj_labela = struct.unpack(">I", fp.read(4))[0]
            # Ucitavanje labela.
            labele = np.frombuffer(fp.read(broj_labela), dtype=np.uint8)
            return labele.astype(np.int64)

    def _load(self, path):
        images_path = path + "-images.idx3-ubyte"
        labels_path = path + "-labels.idx1-ubyte"
        images = self.ucitaj_slike(images_path)
        labels = self.ucitaj_labele(labels_path)
        self.data = list(zip(images.reshape((len(images), 28*28)), labels))
        
    def __getitem__(self, index):
        return self.data[index]
    
    def __len__(self):
        return len(self.data)

test_ds = MnistDataset("t10k")
train_ds = MnistDataset("train")
test_loader = DataLoader(test_ds, shuffle=True, batch_size=5)
train_loader = DataLoader(train_ds, shuffle=True, batch_size=32)

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(784, 500)
        self.fc2 = nn.Linear(500, 250)
        self.fc3 = nn.Linear(250, 10)
        self.fc4 = nn.Linear(10, 250)
        self.fc5 = nn.Linear(250, 500)
        self.fc6 = nn.Linear(500, 784)

    def encode(self, x):
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = F.relu(self.fc3(x))
        return x

    def decode(self, x):
        x = F.relu(self.fc4(x))
        x = F.relu(self.fc5(x))
        x = F.relu(self.fc6(x))
        return x

    def forward(self, x):
        x = self.encode(x)
        x = self.decode(x)
        return x

print(torch.cuda.is_available())

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
net = Net()
net.to(device)

criterion = nn.MSELoss()
optimizer = optim.RMSprop(net.parameters(), lr=0.001)
criterion.to(device)

for epoch in range(15):
    running_loss = 0.0
    for i, data in enumerate(train_loader):
        inputs = data[0].to(device)
        optimizer.zero_grad()
        outputs = net(inputs)
        loss = criterion(outputs, inputs)
        loss.backward()
        optimizer.step()
        running_loss += loss.item()
    print("Gotova epoha {}".format(epoch+1))

print("Obucavanje zavrseno.")

with torch.no_grad():
    inputs, labels = iter(test_loader).next()
    inputs = inputs.to(device)
    encoded = net.encode(inputs)

    # Probati sa nasumice popunjenim vektorom osobina.
    # encoded = torch.from_numpy(np.random.rand(5, 10).astype(np.float32))
    
    encoded = encoded.to(device)
    decoded = net.decode(encoded)
    for i, img in enumerate(chain(inputs, decoded), 1):
        plt.subplot(2, len(inputs), i)
        plt.imshow(img.cpu().reshape((28,28)))
    plt.show()
