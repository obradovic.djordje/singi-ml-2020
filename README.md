# singi-ml-2020

Raspored tema po nedeljama nastave:

## 1) 5.3.2020. četvrtak
    - uvod - veštačka inteligencija, mašinsko učenje
    - primene i motivacija
    - osnove linearne algebre (skalar, vektor, matrica, tenzor, osnovne operacije, razne primene)
    - linearna regresija i linearna klasifikacija

## 2) 12.3.2020. četvrtak
     - osnove statistike i verovatnoće potrebne za ML
     - slučajna promenjiva, raspodele, varijansa, uslovne verovatnoće, bajesova teorema
     - bajesov naivni klasifikator
     - opencv, slike, reprezentacije boja
     - Primer segmentacija slika (pikseli koji reprezentuju boju kože, pikseli neba...)

## 3) 19.3.2020. četvrtak
     - osnove numeričkih metoda, iterativni postupci, gradijentni postupci, optimizacija
     - grupisanje piksela po nekim osobinama, ekstracija povezanih komponenti
     - filter za izdvajanje elemenata koji pripadaju bar kodu (ne samo boja nego i oblik)
     - opencv - video

## 4) 26.3.2020. četvrtak
    - osnove mašinskog učenja
    - regresija, klasifikacija, klasterizacija, metrika
    - KNN, SVM na primeru MNIST data seta

## 5) 2.4.2020. četvrtak
    - duboke neuronske mreže, modeli, obučavanje, optimizacione funkcije
    - sliding window (priprema za konvoluciju)
    - praćenje objekata na  sintetizovanom video snimku
    - primer prepoznavanje MNIST cifara pomoću višeslojnog perceptrona

## 6) 9.4.2020. četvrtak
    - Kolokvijum
    - ocena 6: prepoznavanje MNIST cifara na slici 24x24 pomoću neuronske mreže i KNN klasifikatora
    - ocena 8: prepoznavanje MNIST cifara na velikoj slici pomoću SVM, neuronske mreže i KNN klasifikatora
    - ocena 10: praćenje objekata na sintetizovanom video snimku
## 7) 16.4.2020. četvrtak
    - konvolucija i konvolutivne neuronske mreže
    - primer MNIST mala i velika slika, CIFAR10 i CIFAR100
## 8) 23.4.2020. četvrtak
    - redukcija dimenzionalnosti prostora problema
    - PCA - analiza
    - autoencoder
    - primer klasifikacija regiona od interesa na DICOM slici
## 9) 30.4.2020. četvrtak
    - samo-organizujuće mape
    - unsupervized learning, klasterizacije, DBScan, K-Means, Fuzzy K-Means
    - složene reprezentacije 
    - osnove metričkih prostora, funkcija rastojanja
    - nepreciznosti i neodređenosti

## 10) 7.5.2020. četvrtak
    - reinforcement learning
    - primer igranje igre PONG  

## 11) 14.5.2020. četvrtak
    - detekcija i lokalizacija objekata na slici primernom složenih modela NM 
    - YOLO, RCNN, Faster RCNN

## 12) 21.5.2020. četvrtak
    - Kolokvijum II
    - ocena 6: PCA analiza na sintetičkim primerima
    - ocena 8: detekcija i lokalizacija objekata na slikama velike rezolucije
    - ocena 10: implementirati i opisati najmanje dva različita moldela i sintetizovati ih u jedno rešenje

## 13) 28.5.2020. četvrtak
    - sinteza više različitih izvora podataka, kamera, vizuelna odometrija, 3D
    - Primer: kitti data set
## 14) 4.6.2020. četvrtak
    Zavrsni projekti/ priprema
## 15) 11.6.2020. četvrtak
    Zavrsni projekti/ priprema
